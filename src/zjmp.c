/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   zjmp.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/13 19:02:59 by zbelway           #+#    #+#             */
/*   Updated: 2016/05/22 15:36:31 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

void		call_zjmp(t_vm *vm, int i)
{
	if (!vm->procs[i]->carry)
		return ;
	vm->procs[i]->pc += (vm->procs[i]->instruc.params[0]);
	vm->procs[i]->pc %= MEM_SIZE;
	vm->procs[i]->move_pc = 0;
}
