/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   aff.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/22 18:03:00 by zbelway           #+#    #+#             */
/*   Updated: 2016/05/22 18:35:55 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

char				*ft_dup(char *src, unsigned int value)
{
	char			*tmp;
	unsigned int	i;
	unsigned int	len;

	len = 0;
	while (src[len])
		len++;
	if (!(tmp = malloc(sizeof(char *) * (len + 2))))
		return (NULL);
	i = 0;
	while (src[i] != '\0')
	{
		tmp[i] = src[i];
		i++;
	}
	tmp[i] = value;
	tmp[i + 1] = '\0';
	return (tmp);
}

void				call_aff(t_vm *vm, int i)
{
	unsigned int	len;
	unsigned int	a;
	char			**tmp_buff;

	len = 0;
	while (vm->aff_buffer[len] != NULL)
		len++;
	if (!(tmp_buff = malloc(sizeof(char **) * (len + 2))))
		return ;
	a = 0;
	while (a < len)
	{
		tmp_buff[a] = vm->aff_buffer[a];
		ft_memdel((void **)&vm->aff_buffer[a]);
		a++;
	}
	tmp_buff[i] = ft_dup("Aff: ", get_value_inside_reg((unsigned char *)vm->
				procs[i]->reg[vm->procs[i]->instruc.params[0] - 1]));
	tmp_buff[i + 1] = NULL;
	ft_memdel((void **)&vm->aff_buffer[len]);
	vm->aff_buffer = tmp_buff;
}
