/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   and_or.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 14:48:37 by zbelway           #+#    #+#             */
/*   Updated: 2016/05/22 20:29:48 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

#define AND 1
#define OR 2
#define XOR 3

void	call_and(t_vm *vm, int i)
{
	background_bitwise_op(vm, i, AND);
}

void	call_or(t_vm *vm, int i)
{
	background_bitwise_op(vm, i, OR);
}

void	call_xor(t_vm *vm, int i)
{
	background_bitwise_op(vm, i, XOR);
}
