.name "Godfather"
.comment "just a basic living prog"

sti r1, %:live, %1

live: live %42
fork %:next
and %0, %0, r2 # just call for change carry to 1
next: zjmp %:live
