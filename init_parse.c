/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_parse.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cobrunet <cobrunet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/29 13:37:48 by cobrunet          #+#    #+#             */
/*   Updated: 2016/05/12 14:36:51 by agadhgad         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "init.h"

int		check_array_have(int *t, int len, int what)
{
	int i;
	i = 0;
	while(i < len)
	{
		if (t[i] == what)
			return (0);
		i++;
	}
	return (1);
}

unsigned int	get_prog_length(unsigned char *b, int len)
{
	unsigned char mem[4];
	unsigned int *ptr;
	int a;
	int i;

	if (len < 4)
		return (0);
	a = 0;
	i = 3;
	while (i >= 0)
	{
		mem[a++] = b[i];
		i--;
	}
	ptr = (unsigned int *)mem;
	return (ptr[0]);
}

int		magic_number_is_correct(unsigned char *b, int len)
{
	unsigned char mem[4];
	unsigned int *ptr;
	int a;
	int i;

	if (len < 4)
		return (0);
	a = 0;
	i = 3;
	while (i >= 0)
	{
		mem[a++] = b[i];
		i--;
	}
	ptr = (unsigned int *)mem;
	return ((ptr[0] == COREWAR_EXEC_MAGIC) ? 1 : 0);
}

void	dl_champ(t_champ *champ, char *champ_binary, unsigned int lenchamp)
{
	int i;

	i = 0;
	if (!magic_number_is_correct((unsigned char *)champ_binary, lenchamp))
		exit_with_error("one of you're file have the bad magic number");
	cpy_a_b((void *)(champ->name), (unsigned char *)champ_binary,  4, 4 + PROG_NAME_LENGTH);
	cpy_a_b((void *)(champ->comments), (unsigned char *)champ_binary,  4 + PROG_NAME_LENGTH + 4 + 4, 4 + PROG_NAME_LENGTH + 4 + 4 + COMMENT_LENGTH);
	champ->len_prog = get_prog_length((unsigned char *)champ_binary + 4 + PROG_NAME_LENGTH + 4, lenchamp - 4 - PROG_NAME_LENGTH);
	if (!(4 + PROG_NAME_LENGTH + 4 + 4 + COMMENT_LENGTH + 4 + champ->len_prog == lenchamp))
	{
		ft_printf("%d vs %d\n", 4 + PROG_NAME_LENGTH + 4 + 4 + COMMENT_LENGTH + 4 + champ->len_prog, lenchamp); 
		exit_with_error("you indicate the wrong prog length! dummie!");		
	}
	champ->prog = join_a_b((unsigned char *)champ_binary, 4 + PROG_NAME_LENGTH + 4 + 4 + COMMENT_LENGTH + 4, lenchamp);
	champ->live = 0;
}

/*
a >> 2;
1111 0000 1111 0000
0011 1100 0011 1100
*/
int		set_champions(t_get_procs g, t_vm *vm)
{
	int		i;
	int l1;
	int l2;

	l1 = 0;
	l2 = 0;
	i = 0;
	while (i < g.off1 + g.off2)
	{
		l2 = 0;
		while (l2 < g.off2)
		{
			if (g.players_ordonate[l2] > g.off1 + g.off2)
				exit_with_error("you can't set the position of the champion over the numbers of players!");
			if ((g.players_ordonate[l2] == g.players_natural[l1]) || (g.players_ordonate[l2] == i + 1))
			{
				if (g.players_ordonate[l2] == g.players_natural[l1])
					for (int b = l1-1; b < g.off1; b++)
						g.players_natural[b] += 1;
				dl_champ(&vm->champions[i++], g.t_ordonate[l2], (unsigned)g.len_ordonate[l2]);
				break;
			}
			l2++;
		}
		if (l2 >= g.off2)
		{
			dl_champ(&vm->champions[i++], g.t_natural[l1], (unsigned)g.len_natural[l1]);
			l1++;
		}
	}
	vm->nb_champions = g.off1 + g.off2;
	return (0);
}

int		get_champions(char **av, t_vm *vm)
{
	t_get_procs g;
	int i;

	g.off1 = 0;
	g.off2 = 0;
	g.state = 0;
	i = 0;
	while (i <= MAX_PLAYERS)
	{
		g.players_natural[i] = -1;
		g.t_natural[i] = NULL;
		g.players_ordonate[i] = -1;
		g.t_ordonate[i] = NULL;
		i++;
	}
	g.players_natural[MAX_PLAYERS + 1] = -2;
	g.players_ordonate[MAX_PLAYERS + 1] = -2;
	g.nb = -1;
	i = 0;
	while (av[i] != NULL)
	{
		if (ft_strcmp(av[i], "-dump") == 0 && av[i + 1] != NULL)
		{
			if (g.state == 0)
				vm->nbr_cycle = ft_atoi(av[++i]);
			else
				exit_with_error("I don't want to read two times the dump!");
			g.state = 1;
		}
		else if (ft_strcmp(av[i], "-n") == 0 && av[i + 1] != NULL)
		{
			if (av[i + 2] == NULL)
				exit_with_error("flag \'-n\': usage: -n number champion1.cor");
			g.nb = ft_atoi(av[++i]);
		}
		else if (g.off1 + g.off2 < MAX_PLAYERS)
		{
			if (!file_get_binary(av[i], &g.tmp, &g.lentmp))
				exit_with_error("problem with the file");
			if (g.nb != -1)
			{
				if (g.nb <= 0 || g.nb > MAX_PLAYERS)
					exit_with_error("error whith the -n");
				if (!check_array_have(g.players_ordonate, g.off2, g.nb))
					exit_with_error("I don't want doublon");
				g.players_ordonate[g.off2] = g.nb;
				g.t_ordonate[g.off2] = g.tmp;
				g.len_ordonate[g.off2] = g.lentmp;
				g.off2++;
				g.nb = -1;
			}
			else
			{
				g.players_natural[g.off1] = g.off1 + 1;
				g.t_natural[g.off1] = g.tmp;
				g.len_natural[g.off1] = g.lentmp;
				g.off1++;
			}
		}
		else
			exit_with_error("too many players!");
		i++;
	}
	return (set_champions(g, vm));
}
