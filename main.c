#include <stdio.h>
#include <stdlib.h>

#define REG_SIZE 4
#define REG_LEN 16
/* AREA WHERE I CREATE LITTLE FUNCTION, TRY THEM THEN INTEGRATE IT IN PROG */

void	put_value_inside_reg(unsigned char *reg, unsigned int value)
{
	int i;

	i = REG_SIZE - 1;
	while (i >= 0)
	{
		reg[i] = value % 256;
		value /= 256;
		i--;
	}
}

unsigned int get_value_inside_reg(unsigned char *reg)
{
	int a;
	unsigned int ret;
	unsigned int	zup;

	a = REG_SIZE - 1;
	zup = 1;
	ret = 0;
	while (a >= 0)
	{
		ret += reg[a] * zup;
		zup *= 256;
		a--;
	}
	return (ret);
}

void	print_reg(unsigned char *reg)
{
	int i;

	i = 0;
	while (i < REG_SIZE)
	{
		printf("reg[%d] = %2.2x\n", i, reg[i]);
		i++;
	}

}

void	add_two_reg(unsigned char *reg_dest, unsigned char *reg_a, unsigned char *reg_b)
{
	int i;

	i = REG_SIZE - 1;
	while (i >= 0)
	{
		reg_dest[i] = reg_a[i] + reg_b[i];
		i--;
	}
}

#define MEM_SIZE 10
void		copy_from_mem(unsigned char *dest, unsigned char *mem, unsigned int i)
{
	unsigned int a;

	a = 0;
	while (a < REG_SIZE)
	{
		dest[a] = mem[((i + a) % MEM_SIZE)];
		a++;
	}
}

void		paste_to_mem(unsigned char *src, unsigned char *mem, unsigned int i)
{
	unsigned int a;

	a = 0;
	while (a < REG_SIZE)
	{
		mem[(i + a) % MEM_SIZE] = src[a];
		a++;
	}
}

void	print_mem(unsigned char *mem)
{
	int i;

	i = 0;
	printf("mem:\n");
	while (i < MEM_SIZE)
	{
		printf("%.2hhx ", mem[i]);  
		i++;
	}
	printf("\n");  
}
/*
int main(void)
{
	unsigned char regs[REG_LEN][REG_SIZE];
	unsigned char memory[MEM_SIZE];
	unsigned int	a = 5;
	unsigned int	b = 3;


	put_value_inside_reg((unsigned char *)regs[0], 2000);
	get_value_inside_reg((unsigned char *)regs[0]);
	//add_two_reg((unsigned char *)regs[2], (unsigned char *)regs[0], (unsigned char *)regs[1]);
//	for (int i = 0; i < MEM_SIZE; i++)
//	{
//		memory[i] = i;
//	}
//	print_mem(memory);
//	copy_from_mem((unsigned char *)regs[0], (unsigned char *)memory, 8);
//	print_reg((unsigned char *)regs[0]);
	return (0);
}
*/

int main(int ac, char **av)
{
	unsigned char regs[REG_LEN][REG_SIZE];
	if (ac >= 2)
	{
		put_value_inside_reg((unsigned char *)regs[0], atoi(av[1]));
		get_value_inside_reg((unsigned char *)regs[0]);
	}
}
