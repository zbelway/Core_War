# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: cobrunet <cobrunet@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/03/20 14:36:23 by cobrunet          #+#    #+#              #
#*   Updated: 2016/05/02 11:59:15 by cobrunet         ###   ########.fr       *#
#                                                                              #
# **************************************************************************** #

.PHONY: all clean fclean re

NAME = corewar
CC = gcc
SRC_PATH = ./src/
SRC_NAME = main.c file_get_contents.c print_binary.c init.c init_parse.c \
		   init_parse2.c use.c use2.c r.c \
		join_a_b.c init_mem.c fight.c opcode.c zjmp.c ld.c st.c fork.c and_or.c \
		live_add_sub.c mem_use.c opcode_functions.c aff.c bonus.c print_info.c \
		output.c
CFLAGS = -Wall -Wextra 
LIB = -L./libft/ -lft
LIB_NAME = libft/libft.a
LIB_INC_PATH = libft/inc/
INC_PATH = ./inc/
OBJ_PATH = ./obj/
OBJ_NAME = $(SRC_NAME:.c=.o)

SRC = $(addprefix $(SRC_PATH), $(SRC_NAME))
OBJ = $(addprefix $(OBJ_PATH), $(OBJ_NAME))

all: $(NAME)

$(OBJ_PATH)%.o: $(SRC_PATH)%.c
	@mkdir $(OBJ_PATH) 2> /dev/null || echo "" > /dev/null
	@echo "\033[36mCOREWAR:\033[0m [\033[35mCompilation:\033[0m\033[32m $@\033[0m]"
	@$(CC) -o $@ -c $< $(CFLAGS) -I$(INC_PATH) -I$(LIB_INC_PATH)

$(NAME): $(LIB_NAME) $(OBJ)
	@$(CC) -ltermcap $(CFLAGS) -o $@ $(LIB) $^
	@echo "[\033[36m---------------------------------------\033[0m]"
	@echo "[\033[36m------------- COREWAR - OK ------------\033[0m]"
	@echo "[\033[36m---------------------------------------\033[0m]"

$(LIB_NAME):
	@make -C libft/

clean:
	@rm -rf $(OBJ_PATH)
	@make clean -C libft/

fclean: clean
	@rm -f $(NAME)
	@make fclean -C libft/

re: fclean all
